## Consul
see: https://hub.docker.com/r/gliderlabs/consul/
see: https://www.consul.io/docs/agent/options.html
see: http://artplustech.com/docker-consul-dns-registrator/

Start a single server (should have 3)
  docker run -d -h h_$NODE_NAME \
  -p $NODE_IP:8300:8300 \
  -p $NODE_IP:8301:8301 \
  -p $NODE_IP:8301:8301/udp \
  -p $NODE_IP:8302:8302 \
  -p $NODE_IP:8302:8302/udp \
  -p $NODE_IP:8400:8400 \
  -p $NODE_IP:8500:8500 \
  -p $NODE_IP:53:53 \
  -p $NODE_IP:53:53/udp \
  gliderlabs/consul-server \
  -advertise $NODE_IP \
  -bootstrap \
  -client 0.0.0.0
  -node $NODE_NAME

How did I get to the command above? there is an older version that will help:
  docker run --rm progrium/consul cmd:run $NODE_IP::$CONSUL_IP::client
  docker run --rm progrium/consul cmd:run $NODE_IP::$CONSUL_IP::server

If you want consul-services ignored
	-e SERVICE_IGNORE=1

And then you should tweak the commands you get (https://www.consul.io/docs/agent/options.html).
Consul exposes some services, so there is a lot of port mapping going on, I am not sure if dns port mapping (53).

## Running consul client container
On other nodes, you will typically run a client agent, with options (CONSUL_IP is address of consul server, NODE_IP is address of the node running client agent):
  docker run -d -h h_$NODE_NAME -p 8300:8300 -p 8301:8301 -p 8301:8301/udp -p 8302:8302 -p 8302:8302/udp -p 8400:8400 -p 8500:8500 -p 53:53 -p 53:53/udp gliderlabs/consul-agent -advertise $NODE_IP -join $CONSUL_IP -rejoin -client 0.0.0.0
  docker run -d -h h_$NODE_NAME \
  -p 8300:8300 \
  -p 8301:8301 \
  -p 8301:8301/udp \
  -p 8302:8302 \
  -p 8302:8302/udp \
  -p 8400:8400 \
  -p 8500:8500 \
  -p 53:53 \
  -p 53:53/udp \
  gliderlabs/consul-agent \
  -advertise $NODE_IP \
  -join $CONSUL_IP \
  -rejoin \
  -client 0.0.0.0

## Running consul directly
see: http://blog.tfm.ro/docker-cluster-swarm-with-consul/

Seems easier, because you don't have to fiddle with ports, and you can edit config file ($MY_DIR/config/agent.json)

./consul agent -server -bootstrap-expect 1 -bind $NODE_IP -config-dir=$MY_DIR/config -data-dir=/var/consul -node $DOCKER_MACHINE_NAME

./consul agent -bind $NODE_IP -join $CONSUL_IP -config-dir=$MY_DIR/config -data-dir=/var/consul -node $DOCKER_MACHINE_NAM

$MY_DIR/config/agent.json:
{
	"ui_dir": "/Users/tommy/Downloads/Training/ConsulDocked/consul-ui",
	"log_level": "INFO",
	"enable_syslog": false,
	"retry_join": ["192.168.99.108", "192.168.99.106", "192.168.99.107"],
	"client_addr": "0.0.0.0",
	"leave_on_terminate": true,
	"dns_config": {
		"allow_stale": true,
		"max_stale": "1s"
	}
}



