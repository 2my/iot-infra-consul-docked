Microservice cloud infrastructure setup with docker swarm and consul

# Overview

Consul node

	* consul server

Swarm manager created with swarm options

	* swarm-manage (started automatically)
	* swarm client (started automatically)
	* consul client
	* registrator

Swarm nodes created with swarm options

	* swarm client (started automatically)
	* consul client
	* registrator


The swarm is controlled through manager:
Deploy service to Swarm manager
Swarm manager will put it on some Swarm node
Swarm node registrator will tell Swarm node consul client about the service
Swarm node consul forwards the message to Consul node consul server.

When joining a node to swarm, registry (consul) is signaled with ip of node.


# Quick start
In the following:

	$NODE_IP is ip of the node (where a container is deployed)
	$NODE_NAME is name of the node
	$CONSUL_IP is ip of the consul node (where consul server is deployed)

## Consul server node (consul.io)
Consul is node/service registry, may be used for dns and monitoring later.
Is typically run as a server on separate node, with a client agent on each swarm node.

Create server normally

	docker-machine create -d virtualbox consul
	
Run up server with ip and port-mapping specified:

	docker run -d -h h_$NODE_NAME -p $NODE_IP:8300:8300 -p $NODE_IP:8301:8301 -p $NODE_IP:8301:8301/udp -p $NODE_IP:8302:8302 -p $NODE_IP:8302:8302/udp -p $NODE_IP:8400:8400 -p $NODE_IP:8500:8500 -p $NODE_IP:53:53 -p $NODE_IP:53:53/udp gliderlabs/consul-server -advertise $NODE_IP -bootstrap -client 0.0.0.0 -node $NODE_NAME

UI is accessed on http://$NODE_IP:8500/ui/

## Swarm manager node
Create with swarm options (or suffer tls problems)

	docker-machine create -d virtualbox --swarm --swarm-discovery consul://$CONSUL_IP:8500/sw --swarm-master master

Containers with swarm-manage and swarm client will be started automatically. Do a "docker ps" to check what port the swarm manager listens on. I got SWARM_ADDR = $NODE_IP:3376

## Swarm node
Create with swarm options (or suffer tls problems)

	docker-machine create -d virtualbox --swarm --swarm-discovery consul://$CONSUL_IP:8500/sw node0

Containers with swarm client will be started automatically

## Swarm queries and commands
Check cluster

	docker run swarm list consul://$CONSUL_IP:8500/sw

show running containers in the cluster

	docker -H tcp://$SWARM_ADDR ps

show cluster nodes infos

	docker -H tcp://$SWARM_ADDR info

run containers in the cluster:

	docker -H tcp://$SWARM_ADDR run -d --name www2 -p 81:80 nginx

## Consul client and Registrator (hub.docker.com/r/gliderlabs/registrator/)
Each swarm node needs to run Consul client and Registrator images.
Swarm manager node will also need them, since it doubles as a swarm node as well.

### Consul client

	docker run -d -h h_$NODE_NAME -p 8300:8300 -p 8301:8301 -p 8301:8301/udp -p 8302:8302 -p 8302:8302/udp -p 8400:8400 -p 8500:8500 -p 53:53 -p 53:53/udp gliderlabs/consul-agent -advertise $NODE_IP -join $CONSUL_IP -rejoin -client 0.0.0.0

If you want consul-services ignored, add:

	-e SERVICE_IGNORE=1

### Registrator (hub.docker.com/r/gliderlabs/registrator/)
Small service that attaches to docker daemon and registers services with consul.

	docker run -d -h h_$NODE_NAME -v /var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator consul://$NODE_IP:8500


# More to come

	* https://www.spirulasystems.com/blog/2015/07/02/building-an-automatic-environment-using-consul-and-docker-part-2/
	* https://tech.bellycard.com/blog/load-balancing-docker-containers-with-nginx-and-consul-template/

# Background

Mostly similar: http://howtocookmicroservices.com/docker-compose/

https://docs.docker.com/swarm/install-manual/  
https://www.spirulasystems.com/blog/2015/07/02/building-an-automatic-environment-using-consul-and-docker-part-2/  
http://sirile.github.io/2015/07/28/scaling-with-discovery-on-docker-swarm-with-consul-registrator-and-haproxy-with-prometheus-monitoring-and-elk-log-aggregation.html#using-consul-for-service-discovery-in-the-containers  
http://jlordiales.me/2015/02/03/registrator/  
http://blog.scottlowe.org/2015/03/06/running-own-docker-swarm-cluster/  

Load balancing with nginx and template: https://tech.bellycard.com/blog/load-balancing-docker-containers-with-nginx-and-consul-template/

Docker + Consul + template: https://www.airpair.com/scalable-architecture-with-docker-consul-and-nginx

swarm + consul: http://technologyconversations.com/2015/07/02/scaling-to-infinity-with-docker-swarm-docker-compose-and-consul-part-14-a-taste-of-what-is-to-come/  
Background: http://progrium.com/blog/2014/07/29/understanding-modern-service-discovery-with-docker/  
With dns: http://artplustech.com/docker-consul-dns-registrator/  
http://blog.scottlowe.org/2015/03/06/running-own-docker-swarm-cluster/  

