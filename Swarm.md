# Running up swarm manager

Manager must be created with docker-machine (for tls setup)
http://devopscube.com/how-to-setup-and-configure-docker-swarm-cluster/


## Swarm and tls
see http://devopscube.com/how-to-setup-and-configure-docker-swarm-cluster/
Normally, all nodes in swarm has to use the same certificate for tls and port 2376.
Many articles set up swarm without tls, and port 2375, which may be OK behind firewall.
Without tls you will have to jump some hoops, and I had all kind of problems with this.
Start Docker daemon without tls using:
	docker-machine ssh [node-name]
	sudo docker -H tcp://0.0.0.0:2375 -d &
From docker client you can
	unset DOCKER_TLS_VERIFY

Manually join node in swarm cluster "s2"
	docker run -d swarm join --addr=$NODE_IP:2375 consul://$NODE_IP:8500/s2
Manually start manager for swarm cluster "s2"
	docker run -d -p 3376:2376 swarm manage consul://$NODE_IP:8500/s2

